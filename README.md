# Cook Blog

Cook Blog is a Django-based web application for sharing cooking recipes.

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/django9070852/cook_blog.git
